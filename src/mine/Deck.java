package mine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

public class Deck {

	ArrayList<Card> deck;

	public Deck() {

		shuffle();

	}

	private void newDeck() {
		HashMap<Integer, String> suits = new HashMap<Integer, String>();

		// Maps Integers to suits for use in the deck initialization loops
		// Outer loop tracks suit index, inner loop tracks card index
		suits.put(0, "Diamonds");
		suits.put(1, "Clubs");
		suits.put(2, "Hearts");
		suits.put(3, "Spades");

		deck = new ArrayList<Card>(52);

		for (int i = 0; i < 4; i++) {

			for (int j = 1; i < 14; i++) {

				// if it's an ace
				if (j == 1)
					deck.add(new Card("Ace", suits.get(i)));

				// if it's 2-10
				if (j > 1 && j <= 10)
					deck.add(new Card(new Integer(i).toString(), suits.get(i)));

				// if it's a jack
				if (j == 11)
					deck.add(new Card("Jack", suits.get(i)));
				// if it's a queen
				if (j == 12)
					deck.add(new Card("Queen", suits.get(i)));

				// if it's a king
				if (j == 13)
					deck.add(new Card("King", suits.get(i)));
			}
		}
	}

	public void shuffle() {

		// Reinitializes the deck
		newDeck();

		// Creates a new Random outside of the loop to save time
		Random rand = new Random();

		int newplace;

		// This uses the Fisher-Yates shuffle algorithm
		// It is very efficient and is perfectly "fair" in that each
		// configuration
		// is equally probable
		for (int i = 0; i < deck.size(); i++) {
			newplace = rand.nextInt(deck.size() - i) + i;
			Collections.swap(deck, i, newplace);
		}
	}

	public Card getTop() {

		// takes a copy of the last card and removes it from the deck
		// If there aren't any cards, then the deck.get(deck.size() - 1)
		// throws a nullpointerexception, which causes a card with
		// the word "null" in both fields to be returned
		try {
			Card z = deck.get(deck.size() - 1);
			deck.remove(deck.size() - 1);
			return z;

		} catch (NullPointerException e) {
			return new Card("null", "null");
		}
	}
}
