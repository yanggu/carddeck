package mine;


//This class is made public so that anyone using a Deck object can access the Card object
public class Card {
	private String suit;
	private String label;

	public Card(String label, String suit) {
		this.label = label;
		this.suit = suit;
	}

	public String getSuit() {
		return suit;
	}

	public void setSuit(String suit) {
		this.suit = suit;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}